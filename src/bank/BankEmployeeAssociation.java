/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 * Class BankEmployeeAssociation
 * @author Adeel Khilji
 */
public class BankEmployeeAssociation 
{
    public static void main(String[] args)
    {
        Bank bank = new Bank("TD Canada Trust");//Creating Bank object
        Employee employee = new Employee("Jane Doe");//Creating Employee object

        String format = "BANK NAME: %s\t EMPLOYEE NAME: %s\n\n";//Creating String format
        System.out.printf(format,bank.getName(),employee.getName());//Displaying results
    }
}
