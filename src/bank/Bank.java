/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 * Class Bank
 * @author Adeel Khilji
 */
public class Bank 
{
    private String name;//Declaring instance variable name type String
    /**
     * Constructor with one parameter
     * @param name String
     */
    public Bank(String name)
    {
        this.name = name;//Assigning name to this.name
    }
    /**
     * getName() - getterMethod that returns this.name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
}
