/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 * Class Employee
 * @author Adeel Khilji
 */
public class Employee 
{
    private String name;//Declaring instance variable of type String
    
    /**
     * Constructor with one parameter
     * @param name String
     */
    public Employee(String name)
    {
        this.name = name;
    }
    /**
     * getName() - getterMethod that returns this.name
     * @return String
     */
    public String getName()
    {
        return this.name;
    }
}
